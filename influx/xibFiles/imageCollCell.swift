//
//  imageCollCell.swift
//  influx
//


import UIKit
import SDWebImage
import Foundation

class imageCollCell: UICollectionViewCell {
    @IBOutlet weak var pictures: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpView(int: IndexPath){
        if ImageViewModel.Instance.imageList.count != 0{
        let data = ImageViewModel.Instance.imageList[int.row]
        let imageUrl = ImageViewModel.Instance.getImageUrl(farm:data.farm, server: data.server, id: data.id, secret: data.secret,size: "w")
            pictures.contentMode = .scaleAspectFill
        pictures.sd_setImage(with: imageUrl, placeholderImage: nil)
        pictures.isUserInteractionEnabled = true
        
        }
    
    }

}
