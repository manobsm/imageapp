//
//  ImageFlowModel.swift
//  influx
//


import Foundation

class ImageDetails: NSObject {
    var id = String()
    var owner = String()
    var secret = String()
    var server = String()
    var farm = Int()
    var title = String()
    var ispublic = Int()
    var isfriend = Int()
    var isfamily = Int()
    
    init(id:String, owner:String, secret:String, server:String, farm:Int, title:String, ispublic:Int, isfriend:Int, isfamily:Int) {
        
        self.id = id
        self.owner = owner
        self.secret = secret
        self.server = server
        self.farm = farm
        self.title = title
        self.ispublic = ispublic
        self.isfriend = isfriend
        self.isfamily = isfamily
    
    }
}
