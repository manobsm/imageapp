//
//  Constant.swift
//  influx
//


import Foundation
struct Constants {
    static let BaseUrl = "https://www.flickr.com/services/rest/?method=flickr.groups.pools.getPhotos&api_key="
    static let api_key = "a4f28588b57387edc18282228da39744"
    static let format = "&group_id=1386675%40N21&format=json&nojsoncallback=1&safe_search=1&text=gallery&per_page=20&"
    static let appThemeColor  = UIColor(red: 0.2588235294, green: 0.6901960784, blue: 0.9450980392, alpha: 1.0)
}


