//
//  ViewController.swift
//  influx



import UIKit
import SDWebImage


class ViewController: UIViewController {
    let refreshControl = UIRefreshControl()
    var layout: CustomCollectionViewFlow!
    var lay: RBCollectionViewInfoFolderLayout!
    @IBOutlet weak var picCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView(){
        layout = CustomCollectionViewFlow()
        lay = RBCollectionLayout(view: self.view)
        picCollection.register(collectionViewFolder.self, forSupplementaryViewOfKind: RBCollectionViewInfoFolderFolderKind, withReuseIdentifier: "folder")
        picCollection.register(RBCollectionViewInfoFolderDimple.self, forSupplementaryViewOfKind: RBCollectionViewInfoFolderDimpleKind, withReuseIdentifier: "dimple")
        picCollection.collectionViewLayout = lay
        picCollection.register(UINib.init(nibName: "imageCollCell", bundle: nil), forCellWithReuseIdentifier: "imageCollCell")
        getImageList()
        refreshControl.tintColor = Constants.appThemeColor
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        picCollection.addSubview(refreshControl)
    }
    
    @objc func refreshData() {
        ImageViewModel.Instance.pageCount = 1
        getImageList()
     }
    
    func getImageList(){
        self.showloader(true)
        ImageViewModel.Instance.getFeed{ (status) in
            if status == "1"{
                self.picCollection.reloadData()
                self.refreshControl.endRefreshing()
            }else{
                print("error")
            }
            self.showloader(false)
            
        }
    }
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        print(sender.state.rawValue)
        if sender.state.rawValue == 3{
            self.showPopUpView(false, imageUrl: nil)
        }else{
            let data = ImageViewModel.Instance.imageList[sender.view!.tag]
            let imageUrl = ImageViewModel.Instance.getImageUrl(farm:data.farm, server: data.server, id: data.id, secret: data.secret,size: "z")
            self.showPopUpView(true, imageUrl: imageUrl)
        }
        
    }
  
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, RBCollectionViewInfoFolderLayoutDelegate{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ImageViewModel.Instance.imageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollCell", for: indexPath) as! imageCollCell
        cell.setUpView(int: indexPath)
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        longPressRecognizer.minimumPressDuration = 0.5
        cell.pictures.tag = indexPath.row
        cell.pictures.addGestureRecognizer(longPressRecognizer)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let lay: RBCollectionViewInfoFolderLayout = picCollection.collectionViewLayout as! RBCollectionViewInfoFolderLayout
        lay.toggleFolderView(for: indexPath as IndexPath)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if indexPath.row == ImageViewModel.Instance.imageList.count - 1 {
                ImageViewModel.Instance.pageCount = ImageViewModel.Instance.pageCount + 1
                getImageList()
            }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  3
    }
    
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: RBCollectionViewInfoFolderLayout!, heightForFolderAt indexPath: IndexPath!) -> CGFloat {
        return 150
    }

    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: RBCollectionViewInfoFolderLayout!, sizeForHeaderInSection  section: Int) -> CGSize {
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_  collectionView: UICollectionView!, layout collectionViewLayout: RBCollectionViewInfoFolderLayout!, sizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reuseView: UICollectionReusableView!
        reuseView = picCollection.dequeueReusableCell(withReuseIdentifier: "imageCollCell", for: indexPath) as! imageCollCell
        if(kind == RBCollectionViewInfoFolderFolderKind){
            let colViewFolder: collectionViewFolder = picCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "folder", for: indexPath as IndexPath) as! collectionViewFolder
            let data = ImageViewModel.Instance.imageList[indexPath.row]
            let myview = UIView()
            myview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width-20, height: 150)
            myview.backgroundColor = .blue
            let myLabel = UILabel(frame: CGRect(x: 10, y: 10, width: self.view.frame.width-3, height: 150))
            let mytext = ImageViewModel.Instance.imageInfo(tittle: "Information", size: "\((view.frame.width-25)/2)*\((view.frame.width-25)/2)", information: data.title)
            myLabel.numberOfLines = 0
            myLabel.attributedText = mytext
            myLabel.sizeToFit()
            myview.addSubview(myLabel)
            colViewFolder.addSubview(myview)
            reuseView = colViewFolder
        }
        
        if(kind == RBCollectionViewInfoFolderDimpleKind){
            let dimple: RBCollectionViewInfoFolderDimple = picCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "dimple", for: indexPath as IndexPath) as! RBCollectionViewInfoFolderDimple
            dimple.color = UIColor.white
            reuseView = dimple
        }
        
        return reuseView
    }
}


