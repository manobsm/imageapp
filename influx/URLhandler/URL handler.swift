//
//  URL handler.swift
//  SRIHER
//


import UIKit
import Alamofire
import SystemConfiguration

class URLhandler : NSObject {
    
    static func makeCall(url: String, completionHandler: @escaping (_ responseObject: [String:Any],_ error:Error?  ) -> ()) {
        
        Alamofire.request(url, method: .get, parameters: nil ,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            
            switch response.result {
                case .success:
                    
                let jsonDict = response.result.value as? [String:Any] ?? [:]
                
                print("hitted---->",jsonDict)
                
                completionHandler(response.result.error == nil ? jsonDict : [:] ,response.result.error)
                 
                break
            case .failure(let error):
                
                print(error)
                completionHandler([:],error)
                
                break
            
            }
            
        }
        
    }
 
   
    
}
