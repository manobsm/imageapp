//
//  imagesDataModel.swift
//  influx
//


import Foundation

class ImageViewModel: NSObject {
    static let Instance = ImageViewModel()
    var imageList = [ImageDetails]()
    var pageCount = 1
    
    // image attribute string
    func imageInfo(tittle: String, size: String, information: String) -> NSAttributedString{
        let attrsA = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white,
                      NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        let a = NSMutableAttributedString(string:tittle, attributes:attrsA)
        let attrsB =  [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.white]
        let b = NSAttributedString(string:"\n\n\(size)", attributes:attrsB)
        a.append(b)
        let attrsC =  [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.white]
        let c = NSAttributedString(string:"\n\n\(information)", attributes:attrsC)
        a.append(c)
        return a
    }
    
    func getImageUrl(farm: Int, server: String, id: String, secret: String, size: String) -> URL{
        let url = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_\(size).jpg"
        return URL(string: url)!
    }
    // get feed
    func getFeed(completion: @escaping (_ status:String) -> ()){
        if pageCount == 1{
            self.imageList.removeAll()
        }
        let myurl = "\(Constants.BaseUrl)\(Constants.api_key)\(Constants.format)\("page=\(pageCount)")"
        URLhandler.makeCall(url: myurl, completionHandler: { (response, error) in
            switch error {
            case nil :
                let jsonDict = response
                let Response = jsonDict["photos"] as? [String:Any] ?? [:]
                let status = jsonDict["stat"] as! String
                let data = Response["photo"] as? [[String:Any]] ?? []
                if status == "ok" {
                    _ = data.map({ (val) in
                        self.imageList.append(ImageDetails.init(id: val["id"] as! String, owner: val["owner"] as! String, secret: val["secret"] as! String, server: val["server"] as! String, farm:  val["farm"] as! Int, title: val["title"] as! String, ispublic: val["ispublic"] as! Int, isfriend: val["isfriend"] as! Int, isfamily: val["isfamily"] as! Int))
                    })
                }
                completion("1")
                break
            default:
                
                completion("0")
                break
                
            }
        })
    }
}
