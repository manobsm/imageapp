//
//  Extensions.swift
//
//


import Foundation
import UIKit
import SDWebImage
import NVActivityIndicatorView



extension UIViewController: NVActivityIndicatorViewable  {


    func showloader(_ show : Bool) {
        let size = CGSize(width: 70, height: 70)

        NVActivityIndicatorView.DEFAULT_COLOR = Constants.appThemeColor
        if show {
            self.startAnimating(size, message: "Please wait...", type: .ballScaleMultiple, fadeInAnimation: nil)
        }else{

            self.stopAnimating(nil)
        }
    }
    
    func addChildViewController(childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        addChild(childController)
        holderView?.addSubview(childController.view)
        constrainViewEqual(holderView: holderView!, view: childController.view)
        childController.didMove(toParent: self)
        childController.willMove(toParent: self)
    }
    
    func removeChildView() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
    
    func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
    func showPopUpView(_ show : Bool, imageUrl: URL?) {
        if show{
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            myView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
            myView.isOpaque = false
        myView.tag = 99
        self.view.addSubview(myView)
        let photoView =  UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 100, height: self.view.frame.height/2))
            photoView.backgroundColor = .white
        photoView.layer.cornerRadius = 15
        photoView.center = CGPoint(x: myView.bounds.midX, y: myView.bounds.midY)
        myView.addSubview(photoView)
        let myImage = UIImageView(frame: CGRect(x: 15, y: 15, width: photoView.frame.width - 30, height: photoView.frame.height - 30))
        myImage.center = CGPoint(x: photoView.bounds.midX, y: photoView.bounds.midY)
        myImage.sd_setImage(with: imageUrl, placeholderImage: nil)
            myImage.contentMode = .scaleAspectFit
        photoView.addSubview(myImage)
        }else{
            for subview in self.view.subviews {
            if (subview.tag == 99) {
                subview.removeFromSuperview()
            }
        }
        }
        
    }
    
}

extension UIView {
    func FadeAnimateView(alpha:CGFloat,duration:TimeInterval,delay:TimeInterval, completion: @escaping (_ status:Bool) -> ()) {
        UIView.animate(withDuration: duration, delay: delay, options: .transitionFlipFromTop, animations: {
            self.alpha = alpha
        }, completion: {(isCompleted) in
            completion(true)
        })
    }
    
}
