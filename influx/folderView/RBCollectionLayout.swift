//
//  RBCollectionLayout.swift
//  collectionViewExample
//


import Foundation

class RBCollectionLayout: RBCollectionViewInfoFolderLayout
{
    var view: UIView!
    init(view: UIView){
        super.init()
        self.view = view
        setupLayout()
    }
    
    override init(){
        super.init()
        setupLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout(){
        let numberofItems: CGFloat = 2
        
        let itemWidth = (view.frame.width-25) / numberofItems
        cellSize = CGSize(width: itemWidth, height: itemWidth)
        
        interItemSpacingX = 0
        interItemSpacingY = 5
    }
}
